CC = g++
CFLAGS = `pkg-config --cflags opencv4`
LDFLAGS = `pkg-config --libs opencv4` -lwiringPi
EXECUTABLE = completo3

all: $(EXECUTABLE)

$(EXECUTABLE): completo3.cpp
	$(CC) $(CFLAGS) $< -o $@ $(LDFLAGS)

.PHONY: run
run:
	./$(EXECUTABLE) faca_cascade.xml olho_cascade.xml

.PHONY: clean
clean:
	rm -f $(EXECUTABLE)
